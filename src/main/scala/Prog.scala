// src/main/scala/Prog.scala

object Prog {
	import akka.actor._

	case class Init (act : ActorRef, actorsLeft : Int)
	case object Msg

	class MyActor extends Actor {

		var nextInQueue : Option[ActorRef] = None
		var haveRecieved = false

		def receive: Receive = {
			case initMsg : Init => {
				println("Recieved init")
				if(initMsg.actorsLeft > 0) {
					nextInQueue = Option(context.system.actorOf(Props[MyActor], "Actor_" + initMsg.actorsLeft.toString))
					nextInQueue.get ! Init(initMsg.act, initMsg.actorsLeft - 1)
				} else {
					nextInQueue = Option(initMsg.act)
					nextInQueue.get ! Msg
				}
			}
			case Msg => {
				if(nextInQueue.isEmpty) {

				} else {
					val next = nextInQueue.get
					if(haveRecieved) {
						context.system.terminate()
					} else {
						haveRecieved = true
						println(s"recieved by $self")
						next ! Msg
					}
				}
			}
		}
	}
	def main(args: Array[String]): Unit = {
		import akka.actor.ActorDSL._
		val system = ActorSystem("system")
		val leonardo = system.actorOf(Props[MyActor], "Actor_0")
		leonardo ! Init(leonardo, 5)
	}
}
